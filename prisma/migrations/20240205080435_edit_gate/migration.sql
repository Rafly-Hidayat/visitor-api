/*
  Warnings:

  - You are about to drop the column `residentialId` on the `gates` table. All the data in the column will be lost.
  - Added the required column `residentId` to the `gates` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "gates" DROP CONSTRAINT "gates_residentialId_fkey";

-- AlterTable
ALTER TABLE "gates" DROP COLUMN "residentialId",
ADD COLUMN     "residentId" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "gates" ADD CONSTRAINT "gates_residentId_fkey" FOREIGN KEY ("residentId") REFERENCES "residents"("residentId") ON DELETE RESTRICT ON UPDATE CASCADE;
