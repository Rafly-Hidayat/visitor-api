/*
  Warnings:

  - Added the required column `gateInId` to the `visitors` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "visitors" ADD COLUMN     "gateInId" TEXT NOT NULL,
ADD COLUMN     "gateOutId" TEXT;

-- AddForeignKey
ALTER TABLE "visitors" ADD CONSTRAINT "visitors_gateInId_fkey" FOREIGN KEY ("gateInId") REFERENCES "gates"("gateId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "visitors" ADD CONSTRAINT "visitors_gateOutId_fkey" FOREIGN KEY ("gateOutId") REFERENCES "gates"("gateId") ON DELETE SET NULL ON UPDATE CASCADE;
