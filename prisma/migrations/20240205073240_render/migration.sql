/*
  Warnings:

  - You are about to drop the column `resident` on the `visitors` table. All the data in the column will be lost.
  - Added the required column `residentId` to the `visitors` table without a default value. This is not possible if the table is not empty.
  - Made the column `status` on table `visitors` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterEnum
ALTER TYPE "Role" ADD VALUE 'SECURITY';

-- AlterTable
ALTER TABLE "visitors" DROP COLUMN "resident",
ADD COLUMN     "residentId" TEXT NOT NULL,
ALTER COLUMN "status" SET NOT NULL;

-- CreateTable
CREATE TABLE "residents" (
    "residentId" TEXT NOT NULL,
    "name" VARCHAR(100) NOT NULL,
    "address" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "residents_pkey" PRIMARY KEY ("residentId")
);

-- CreateTable
CREATE TABLE "gates" (
    "gateId" TEXT NOT NULL,
    "name" VARCHAR(100) NOT NULL,
    "residentialId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "gates_pkey" PRIMARY KEY ("gateId")
);

-- AddForeignKey
ALTER TABLE "visitors" ADD CONSTRAINT "visitors_residentId_fkey" FOREIGN KEY ("residentId") REFERENCES "residents"("residentId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "gates" ADD CONSTRAINT "gates_residentialId_fkey" FOREIGN KEY ("residentialId") REFERENCES "residents"("residentId") ON DELETE RESTRICT ON UPDATE CASCADE;
