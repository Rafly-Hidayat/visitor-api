import express from "express";
import visitorController from "../controller/visitor-controller.js";
import { upload } from "../middleware/multer.js";
import userController from "../controller/user-controller.js";
import purposeController from "../controller/purpose-controller.js";
import residentController from "../controller/resident-controller.js";
import gateController from "../controller/gate-controller.js";

const router = express.Router();

// auth
router.post("/register", userController.registerUser);
router.post("/login", userController.login);

// user
router.get("/user", userController.getAll);
router.post("/user", userController.registerUser);

// visitor
router.post("/visitor", upload.single("idCard"), visitorController.create);
router.get("/visitor", visitorController.getAll);
router.get("/visitor/:visitorId", visitorController.getById);
router.post("/visitor/out/:visitorId", visitorController.out);
router.get("/visitor/idCard/:visitorId", visitorController.getIdCard);

// purpose
router.post("/purpose", purposeController.create);
router.get("/purpose", purposeController.getAll);
router.post("/purpose/update/:purposeId", purposeController.update);
router.post("/purpose/delete/:purposeId", purposeController.remove);

// resident
router.post("/resident", residentController.create);
router.get("/resident", residentController.getAll);
router.get("/resident/:residentId", residentController.getById);
router.post("/resident/update/:residentId", residentController.update);
router.post("/resident/delete/:residentId", residentController.remove);
router.get("/resident/qrcode/:residentId", residentController.getQR);

// gate
router.post("/gate", gateController.create);
router.get("/gate", gateController.getAll);
router.get("/gate/:gateId", gateController.getById);
router.post("/gate/update/:gateId", gateController.update);
router.post("/gate/delete/:gateId", gateController.remove);
router.get("/gate/qrcode/:gateId", gateController.getQR);

export default router;
