import userService from "../services/user-service.js";

const registerUser = async (req, res, next) => {
    try {
        const role = req.body.role || "USER"
        req.body.role = role

        const result = await userService.registerUser(req.body)
        res.status(200).json({
            message: "Successfully register a user",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const login = async (req, res, next) => {
    try {
        const result = await userService.login(req.body)
        res.status(200).json({
            message: "Successfully logged in",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const getAll = async (req, res, next) => {
    try {
        const result = await userService.getAll()
        res.status(200).json({
            message: "Successfully get all user",
            data: result,
        })
    } catch (error) {
        next(error);
    }
}

export default { registerUser, login, getAll }
