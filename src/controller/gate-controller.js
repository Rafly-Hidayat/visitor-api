import gateService from "../services/gate-service.js";

const create = async (req, res, next) => {
    try {
        const result = await gateService.create(req.body)
        res.status(200).json({
            message: "Successfully create gate",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const getAll = async (req, res, next) => {
    try {
        const result = await gateService.getAll()
        res.status(200).json({
            message: "Successfully get all gate",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const update = async (req, res, next) => {
    try {
        const gateId = req.params.gateId
        const request = req.body
        request.gateId = gateId

        const result = await gateService.update(request)
        res.status(200).json({
            message: "Successfully update gate",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const remove = async (req, res, next) => {
    try {
        await gateService.remove(req.params.gateId)
        res.status(200).json({
            message: "Successfully remove gate",
            data: null
        })
    } catch (error) {
        next(error);
    }
}

const getQR = async (req, res, next) => {
    try {
        const result = await gateService.getQR(req.params.gateId)
        res.status(200).json({
            message: "Successfully get qrcode gate",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const getById = async (req, res, next) => {
    try {
        const result = await gateService.getById(req.params.gateId)
        res.status(200).json({
            message: "Successfully get by id gate",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

export default { create, getAll, update, remove, getQR, getById }
