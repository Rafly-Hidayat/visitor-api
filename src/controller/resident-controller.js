import residentService from "../services/resident-service.js";

const create = async (req, res, next) => {
    try {
        const result = await residentService.create(req.body)
        res.status(200).json({
            message: "Successfully create resident",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const getAll = async (req, res, next) => {
    try {
        const result = await residentService.getAll()
        res.status(200).json({
            message: "Successfully get all resident",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const update = async (req, res, next) => {
    try {
        const residentId = req.params.residentId
        const request = req.body
        request.residentId = residentId

        const result = await residentService.update(request)
        res.status(200).json({
            message: "Successfully update resident",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const remove = async (req, res, next) => {
    try {
        await residentService.remove(req.params.residentId)
        res.status(200).json({
            message: "Successfully remove resident",
            data: null
        })
    } catch (error) {
        next(error);
    }
}

const getQR = async (req, res, next) => {
    try {
        const result = await residentService.getQR(req.params.residentId)
        res.status(200).json({
            message: "Successfully get qrcode resident",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

const getById = async (req, res, next) => {
    try {
        const result = await residentService.getById(req.params.residentId)
        res.status(200).json({
            message: "Successfully get by id resident",
            data: result
        })
    } catch (error) {
        next(error);
    }
}

export default { create, getAll, update, remove, getQR, getById }
