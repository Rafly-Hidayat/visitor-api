import { createCanvas, loadImage } from 'canvas';
import qr from 'qrcode';
import prisma from "../database.js"
import { ResponseError } from "../err/err-response.js"
import { createResidentValidation, updateResidentValidation } from "../validation/resident-schema.js"
import { validation } from "../validation/validation.js"

const create = async (request) => {
    const data = validation(createResidentValidation, request)

    const countResident = await prisma.resident.count({
        where: { name: data.name, address: data.address }
    })

    if (countResident > 0) {
        throw new ResponseError(400, "Resident already exists")
    }

    return prisma.resident.create({
        data
    })
}

const getAll = async () => {
    return prisma.resident.findMany({
        include: { Gate: true }
    })
}

const update = async (request) => {
    const data = validation(updateResidentValidation, request)

    const findResident = await prisma.resident.findUnique({
        where: { residentId: data.residentId }
    })

    if (!findResident) {
        throw new ResponseError(404, "Resident not found")
    }

    const countResident = await prisma.resident.count({
        where: { name: data.name, address: data.address }
    })

    if (countResident > 0) {
        throw new ResponseError(400, "Resident already exists")
    }

    return prisma.resident.update({
        data,
        where: { residentId: findResident.residentId }
    })
}

const remove = async (residentId) => {
    const findResident = await prisma.resident.findUnique({
        where: { residentId }
    })

    if (!findResident) {
        throw new ResponseError(404, "Resident not found")
    }

    await prisma.resident.delete({
        where: { residentId }
    })
}

const getQR = async (residentId) => {
    const findResident = await prisma.resident.findUnique({
        where: { residentId }
    })

    const text = findResident.name;

    const qrCode = await qr.toDataURL(findResident.residentId, { errorCorrectionLevel: 'H' });

    // Load QR code image onto canvas
    const canvas = createCanvas(310, 310);
    const ctx = canvas.getContext('2d');
    const qrImage = await loadImage(qrCode);
    ctx.drawImage(qrImage, 0, 0, 310, 310);

    // Add additional text at the bottom
    ctx.font = '16px Arial';
    ctx.fillStyle = 'black';
    ctx.textAlign = 'center';
    ctx.fillText(text, 150, 300);

    return canvas.toDataURL()
}

const getById = async (residentId) => {
    const findGate = await prisma.resident.findUnique({
        where: { residentId },
        include: { Gate: true }
    })

    if (!findGate) {
        throw new ResponseError(404, "Resident not found")
    }

    return findGate;
}

export default { create, getAll, update, remove, getQR, getById }
