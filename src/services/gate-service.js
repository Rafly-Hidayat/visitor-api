import { createCanvas, loadImage } from 'canvas';
import qr from 'qrcode';
import prisma from "../database.js"
import { ResponseError } from "../err/err-response.js"
import { createGateValidation, updateGateValidation } from "../validation/gate-schema.js"
import { validation } from "../validation/validation.js"

const create = async (request) => {
    const data = validation(createGateValidation, request)

    const findResident = await prisma.resident.findFirst({
        where: { name: data.resident }
    })

    if (!findResident) {
        throw new ResponseError(404, "Resident not found")
    }

    const countGate = await prisma.gate.count({
        where: { name: data.name, residentId: findResident.residentId }
    })

    if (countGate > 0) {
        throw new ResponseError(400, "Gate already exists")
    }

    data.residentId = findResident.residentId
    delete data.resident

    return prisma.gate.create({
        data
    })
}

const getAll = async () => {
    return prisma.gate.findMany({
        include: { resident: true }
    })
}

const update = async (request) => {
    const data = validation(updateGateValidation, request)

    const findGate = await prisma.gate.findUnique({
        where: { gateId: data.gateId }
    })

    if (!findGate) {
        throw new ResponseError(404, "Gate not found")
    }

    const findResident = await prisma.resident.findFirst({
        where: { name: data.resident }
    })

    if (!findResident) {
        throw new ResponseError(404, "Resident not found")
    }

    const countGate = await prisma.gate.count({
        where: { name: data.name, residentId: findResident.residentId }
    })

    if (countGate > 0) {
        throw new ResponseError(400, "Gate already exists")
    }

    data.residentId = findResident.residentId
    delete data.resident

    return prisma.gate.update({
        data,
        where: { gateId: findGate.gateId }
    })
}

const remove = async (gateId) => {
    const findGate = await prisma.gate.findUnique({
        where: { gateId }
    })

    if (!findGate) {
        throw new ResponseError(404, "Gate not found")
    }

    await prisma.gate.delete({
        where: { gateId }
    })
}

const getQR = async (gateId) => {
    const findGate = await prisma.gate.findUnique({
        where: { gateId },
        include: { resident: true }
    })

    if (!findGate) {
        throw new ResponseError(404, "Gate not found")
    }

    const text = `${findGate.resident.name} - ${findGate.name}`;

    const qrCode = await qr.toDataURL(findGate.gateId, { errorCorrectionLevel: 'H' });

    // Load QR code image onto canvas
    const canvas = createCanvas(310, 310);
    const ctx = canvas.getContext('2d');
    const qrImage = await loadImage(qrCode);
    ctx.drawImage(qrImage, 0, 0, 310, 310);

    // Add additional text at the bottom
    ctx.font = '16px Arial';
    ctx.fillStyle = 'black';
    ctx.textAlign = 'center';
    ctx.fillText(text, 150, 300);

    return canvas.toDataURL()
}

const getById = async (gateId) => {
    const findGate = await prisma.gate.findUnique({
        where: { gateId },
        include: { resident: true }
    })

    if (!findGate) {
        throw new ResponseError(404, "Gate not found")
    }

    return findGate;
}

export default { create, getAll, update, remove, getQR, getById }
