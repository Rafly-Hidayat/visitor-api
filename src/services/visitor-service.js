import { createCanvas, loadImage } from 'canvas';
import qr from 'qrcode';
import prisma from "../database.js";
import { ResponseError } from "../err/err-response.js";
import { validation } from "../validation/validation.js";
import { createVisitorValidation, outVisitorValidation } from "../validation/visitor-schema.js";

const create = async (request) => {
    const data = validation(createVisitorValidation, request)

    const findPurpose = await prisma.purpose.findFirst({
        where: { purpose: data.purpose }
    })

    if (!findPurpose) {
        throw new ResponseError(404, "Purpose not found")
    }

    data.purposeId = findPurpose.purposeId;
    delete data.purpose

    const findResident = await prisma.resident.findFirst({
        where: { name: data.resident },
        include: { Gate: true }
    })

    if (!findResident) {
        throw new ResponseError(404, "Resident not found")
    }

    data.residentId = findResident.residentId;
    delete data.resident

    const findGateInResident = findResident.Gate.find(gate => gate.name === data.gateIn)
    if (!findGateInResident) {
        throw new ResponseError(404, "Resident Gate not found")
    }

    data.gateInId = findGateInResident.gateId;
    delete data.gateIn

    return prisma.visitor.create({
        data
    })
}

const out = async (request) => {
    const data = validation(outVisitorValidation, request)

    const visitor = await prisma.visitor.findUnique({
        where: { visitorId: data.visitorId, dateOut: null },
        include: { resident: true }
    })

    if (!visitor) {
        throw new ResponseError(404, "visitor not found")
    }

    const findResident = await prisma.resident.findUnique({
        where: { residentId: visitor.residentId },
        include: { Gate: true }
    })

    const findGateInResident = findResident.Gate.find(gate => gate.name === data.gateOut)
    if (!findGateInResident) {
        throw new ResponseError(404, "Resident Gate not found")
    }

    data.gateOutId = findGateInResident.gateId;
    delete data.gateOut

    data.status = "OUTSIDE"

    return prisma.visitor.update({
        data,
        where: { visitorId: visitor.visitorId },
    })
}

const getAll = async () => {
    return prisma.visitor.findMany({
        include: { purpose: true, resident: true, gateIn: true, gateOut: true },
    })
}

const getIdCard = async (visitorId) => {
    const visitor = await prisma.visitor.findUnique({
        where: { visitorId },
    })

    if (!visitor) {
        throw new ResponseError(404, "visitor not found")
    }

    return visitor.idCard
}

const getById = async (visitorId) => {
    const visitor = await prisma.visitor.findUnique({
        where: { visitorId },
        include: { purpose: true, resident: true, gateIn: true, gateOut: true }
    })

    if (!visitor) {
        throw new ResponseError(404, "visitor not found")
    }

    const text = visitor.dateIn;

    const qrCode = await qr.toDataURL(visitor.visitorId, { errorCorrectionLevel: 'H' });

    // Load QR code image onto canvas
    const canvas = createCanvas(310, 310);
    const ctx = canvas.getContext('2d');
    const qrImage = await loadImage(qrCode);
    ctx.drawImage(qrImage, 0, 0, 310, 310);

    // Add additional text at the bottom
    ctx.font = '16px Arial';
    ctx.fillStyle = 'black';
    ctx.textAlign = 'center';
    ctx.fillText(text, 150, 300);

    return { ...visitor, qrcode: canvas.toDataURL() }
}

export default { create, out, getAll, getIdCard, getById }
