import joi from 'joi';

const createGateValidation = joi.object({
    name: joi.string().max(100).required(),
    resident: joi.string().max(100).required(),
})

const updateGateValidation = joi.object({
    gateId: joi.string().required(),
    name: joi.string().max(100).required(),
    resident: joi.string().max(100).required(),
})

export { createGateValidation, updateGateValidation }
