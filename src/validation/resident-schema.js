import joi from 'joi';

const createResidentValidation = joi.object({
    name: joi.string().max(100).required(),
    address: joi.string().max(100).required(),
})

const updateResidentValidation = joi.object({
    residentId: joi.string().required(),
    name: joi.string().max(100).required(),
    address: joi.string().max(100).required(),
})

export { createResidentValidation, updateResidentValidation }
